﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntityIndicator : MonoBehaviour
{
    public Transform entity;
    public Texture2D icon;
    public Texture2D arrow;

    // Update is called once per frame
    void Update()
    {
        Bounds cameraBounds = Camera.main.OrthographicBounds();
        if (!cameraBounds.Contains(entity.position))
        {
        }
    }
}
