﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartHudController : MonoBehaviour
{
    public float timeDisplayed = 1f;
    public float fadeOutTime = .5f;

    private float _timeSinceAwake = 0f;
    private bool _isFadingOut = false;
    private CanvasGroup _canvasGroup;
    void Start()
    {
        TextMeshProUGUI text = GetComponentInChildren<TextMeshProUGUI>();
        text.SetText(SceneManager.GetActiveScene().name.Insert("Level".Length, " "));
        _canvasGroup = GetComponentInChildren<CanvasGroup>();
    }

    void Update()
    {
        _timeSinceAwake += Time.deltaTime;
        if(_timeSinceAwake > timeDisplayed && !_isFadingOut)
        {
            _isFadingOut = true;
        }
        if(_isFadingOut)
        {
            _canvasGroup.alpha -= Time.deltaTime / fadeOutTime;
        }
    }
}
