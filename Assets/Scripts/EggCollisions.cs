﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EggCollisions : MonoBehaviour
{
    public Collider2D floor;
    public Sprite fine;
    public Sprite broken;
    public ParticleSystem particle;
    public AudioSource eggCrack;
    public AudioSource eggHit;

    private Collider2D _self;
    private SpriteRenderer _renderer;
    private bool _isBroken = false;

    void Start()
    {
        _self = GetComponent<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider == floor && !_isBroken)
        {
            eggCrack.Play();
            _isBroken = true;
            _renderer.sprite = broken;
            if (particle != null)
            {
                particle.Play();
                particle.Emit(100);
            }
        }
        else
        {
            if (!eggHit.isPlaying)
            {
                eggHit.Play();
            }
        }
    }

    public bool IsBroken()
    {
        return _isBroken;
    }
}
