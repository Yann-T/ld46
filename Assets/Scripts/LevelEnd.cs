﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelEnd : MonoBehaviour
{
    public GameObject playerEntity;
    public GameObject eggEntity;
    public string winSceneName;
    public string failSceneName;
    public float timeBeforeRestartFail = 1f;
    public float timeBeforeRestartWin = .5f;
    public GameObject hud;

    private Collider2D _selfCollider;
    private Collider2D _eggCollider;
    private Collider2D _playerCollider;
    private EggCollisions _eggCollisions;
    private float _endTime;
    private bool _gameEnded = false;
    private bool _won = false;

    void Start()
    {
        _selfCollider = GetComponent<Collider2D>();
        _eggCollider = eggEntity.GetComponent<Collider2D>();
        _playerCollider = playerEntity.GetComponent<Collider2D>();
        _eggCollisions = eggEntity.GetComponent<EggCollisions>();
    }

    private void FixedUpdate()
    {
        if(_gameEnded)
        {
            _endTime -= Time.deltaTime;
            if(_endTime <= 0)
            {
                if (_won)
                {
                    SceneManager.LoadScene(winSceneName);
                }
                else
                {
                    SceneManager.LoadScene(failSceneName);
                }
            }
        }

        if(_eggCollisions.IsBroken() && !_gameEnded)
        {
            _gameEnded = true;
            _endTime = timeBeforeRestartFail;
        }

        if(_selfCollider.IsTouching(_playerCollider) &&
            _selfCollider.IsTouching(_eggCollider) &&
            !_gameEnded)
        {
            _gameEnded = true;
            _won = true;
            _endTime = timeBeforeRestartWin;
            SaveHighScore();
        }
    }

    private void SaveHighScore() {
        Timer timer = hud.GetComponent<Timer>();
        timer.Pause();
        if (PlayerPrefs.HasKey("PB_" + SceneManager.GetActiveScene().name))
        {
            if(PlayerPrefs.GetFloat("PB_" + SceneManager.GetActiveScene().name) > timer.GetCurrentTime())
            {
                PlayerPrefs.SetFloat("PB_" + SceneManager.GetActiveScene().name, timer.GetCurrentTime());
            }
        } else
        {
            PlayerPrefs.SetFloat("PB_" + SceneManager.GetActiveScene().name, timer.GetCurrentTime());
        }
    }
}
