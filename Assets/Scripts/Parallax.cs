﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    private float lenght, startposX, startposY;

    public float parallaxEffect;
    
    void Start()
    {
        startposX = transform.position.x;
        startposY = transform.position.y;
        lenght = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        float distance = Camera.main.transform.position.x * parallaxEffect;

        transform.position = new Vector3(startposX + distance, transform.position.y, transform.position.z);
    }
}
