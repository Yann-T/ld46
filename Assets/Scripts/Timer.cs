﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    float _currentTime = 0f;
    bool _isPaused = false;

    void Update()
    {
        if(!_isPaused)
        {
            _currentTime += Time.deltaTime;
        }
    }

    public void Pause()
    {
        _isPaused = true;
    }

    public void Resume()
    {
        _isPaused = false;
    }

    public void Stop()
    {
        _currentTime = 0f;
        Pause();
    }


    public float GetCurrentTime()
    {
        return _currentTime;
    }
}
