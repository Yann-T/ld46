﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 500f;
    public float rotationSpeed = 100f;
    public float maxSpeed = 200f;
    public Collider2D[] wheelsColliders;
    public Collider2D floorCollider;
    public Animator animator;

    Rigidbody2D _rigidbody;

    void Start()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    private void FixedUpdate()
    {
        float horizontalValue = 0f;
        horizontalValue = Input.GetAxis("Horizontal") * Time.deltaTime;
        if (Input.touchCount > 0)
        {
            for(int i = 0; i < Input.touchCount; i++)
            {
                Touch t = Input.GetTouch(i);
                if(t.position.x > Screen.width/2)
                {
                    horizontalValue += Time.deltaTime; ;
                    if(horizontalValue > Time.deltaTime)
                    {
                        horizontalValue = Time.deltaTime;
                    }
                } else
                {
                    horizontalValue -= Time.deltaTime;
                    if (horizontalValue < -Time.deltaTime)
                    {
                        horizontalValue = -Time.deltaTime;
                    }
                }
            }
        }

        Vector2 acceleration = new Vector2(horizontalValue * moveSpeed, 0);

        if (transform.rotation.z > 0 && horizontalValue > 0)
        {
            acceleration *= 1f- transform.rotation.z;
        }

        if (transform.rotation.z < 0 && horizontalValue < 0)
        {
            acceleration *= 1f + transform.rotation.z;
        }

        bool onTheGround = false;
        foreach(Collider2D wheel in wheelsColliders)
        {
            if(wheel.IsTouching(floorCollider))
            {
                onTheGround = true;
            }
        }
        if (onTheGround)
        {
            _rigidbody.AddRelativeForce(acceleration);
            _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, maxSpeed);
        } else
        {
            _rigidbody.AddTorque(horizontalValue * rotationSpeed);
        }
        if(_rigidbody.velocity.magnitude >= 0)
        {
            animator.speed = _rigidbody.velocity.magnitude / 5;
            if (_rigidbody.velocity.x > 0)
            {
                animator.SetBool("MovingRight", true);
                animator.SetBool("MovingLeft", false);
            } else
            {
                animator.SetBool("MovingLeft", true);
                animator.SetBool("MovingRight", false);
            }
        } else
        {
            animator.SetBool("MovingLeft", false);
            animator.SetBool("MovingRight", false);
        }
    }
}
