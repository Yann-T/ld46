﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HUDManager : MonoBehaviour
{
    public Timer timer;
    public TextMeshProUGUI currentTimeDisplay;
    public TextMeshProUGUI pbDisplay;
    public 

    void Start()
    {

        // Get pb from ScoreBoard class
        if(PlayerPrefs.HasKey("PB_" + SceneManager.GetActiveScene().name))
        {
            TimeSpan time = TimeSpan.FromSeconds(PlayerPrefs.GetFloat("PB_" + SceneManager.GetActiveScene().name));
            pbDisplay.SetText(time.ToString("mm':'ss'.'fff"));
        } else
        {
            pbDisplay.SetText("--:--.---");
        }
        currentTimeDisplay.SetText("00:00:000");
    }

    // Update is called once per frame
    void Update()
    {
        TimeSpan timespan = TimeSpan.FromSeconds(timer.GetCurrentTime());
        currentTimeDisplay.SetText(timespan.ToString("mm':'ss'.'fff"));
    }
}
