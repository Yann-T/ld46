﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelsMenu : MonoBehaviour
{
    public GameObject scrollViewContent;
    public GameObject elements;

    void Start()
    {
        PopulateScrollView();
    }

    private void PopulateScrollView()
    {
        for (int i = 1; i < SceneManager.sceneCountInBuildSettings; i++)
        {
            GameObject elt = Instantiate(elements, scrollViewContent.transform);
            TextMeshProUGUI level = elt.GetComponentsInChildren<TextMeshProUGUI>()[0];
            TextMeshProUGUI time = elt.GetComponentsInChildren<TextMeshProUGUI>()[1];
            level.SetText("Level " + i);
            Debug.Log("PB_" + GetSceneNameByBuildIndex(i));
            if (PlayerPrefs.HasKey("PB_" + GetSceneNameByBuildIndex(i)))
            {
                TimeSpan pb = TimeSpan.FromSeconds(PlayerPrefs.GetFloat("PB_" + GetSceneNameByBuildIndex(i)));
                time.SetText(pb.ToString("mm':'ss'.'fff"));
            }
            else
            {
                time.SetText("--:--.---");
            }
            Button btn = elt.GetComponent<Button>();
            int v = i;
            btn.onClick.AddListener(delegate { PlayLevel(v); });
        }
    }

    private string GetSceneNameByBuildIndex(int buildIndex)
    {
        string scenePath = SceneUtility.GetScenePathByBuildIndex(buildIndex);
        var sceneNameStart = scenePath.LastIndexOf("/", StringComparison.Ordinal) + 1;
        var sceneNameEnd = scenePath.LastIndexOf(".", StringComparison.Ordinal);
        var sceneNameLength = sceneNameEnd - sceneNameStart;
        return scenePath.Substring(sceneNameStart, sceneNameLength);
    }

    public void PlayLevel(int number)
    {
        SceneManager.LoadScene("level" + number);
    }
}
